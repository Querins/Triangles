package com.softserve.edu;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        List<Triangle> triangles = new ArrayList<Triangle>();
        Scanner sc = new Scanner(System.in);
        sc.useDelimiter("\\s*");
        String answer;

        System.out.println("Type triangle parameters in format <имя>, <длина стороны>, <длина стороны>, <длина стороны>");

        do {

            String s = sc.nextLine();
            Triangle t = TriangleParser.parseTriangle(s);
            triangles.add(t);
            System.out.println("Do you want to continue? Type y or yes");
            answer = sc.nextLine();

        } while(answer.equalsIgnoreCase("yes") || answer.equalsIgnoreCase("y"));

        triangles.sort((a,b) -> {
            if(a.area() > b.area()) {
                return -1;
            } else {
                return 1;
            }
        });

        System.out.println("============= Triangles list: ===============");

        for(int i = 0; i < triangles.size(); i++) {
            System.out.format("%d. %s\n", i + 1, triangles.get(i).toString());
        }

    }

}
