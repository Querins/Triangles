package com.softserve.edu;

public class TriangleParser {

    public static Triangle parseTriangle(String str) {

        String[] arguments = str.split("\\s*,");
        double a = Double.parseDouble(arguments[1]);
        double b = Double.parseDouble(arguments[2]);
        double c = Double.parseDouble(arguments[3]);

        return new Triangle(arguments[0], a, b, c);

    }

}
