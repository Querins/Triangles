package com.softserve.edu;

public class Triangle {

    private double a;
    private double b;
    private double c;
    private String name;

    public Triangle(String name, double a, double b, double c) {
        setSize(a,b,c);
        setName(name);
    }

    public Triangle() {
        this("", 0, 0, 0);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setSize(double a, double b, double c) {

        if(a < 0 || b < 0 || c < 0 ) throw new IllegalArgumentException("Wrong arguments");
        this.a = a;
        this.b = b;
        this.c = c;

    }

    public double area() {

        double p = (a + b + c) / 2;
        double area = Math.sqrt( p * (p - a) * (p-b) * (p-c) );
        return area;

    }

    @Override
    public String toString() {

        return String.format("[Triangle %s]: %fcm", getName(), area());

    }

}
